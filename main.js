/**
 * Created by mrtrom on 10/4/16.
 */

'use strict';

const kue = require('kue');
const express = require('express');
const ui = require('kue-ui');
const config = require('config');
let app = express();

// connect kue to appropriate redis
kue.createQueue({
  redis: config.redis.url
});

ui.setup({
  apiURL: config.api.apiURL, // IMPORTANT: specify the api url
  baseURL: config.api.baseURL, // IMPORTANT: specify the base url
  updateInterval: config.api.updateInterval // Optional: Fetches new data every 5000 ms
});

// Mount kue JSON api
app.use(config.api.apiURL, kue.app);

// Mount UI
app.use(config.api.baseURL, ui.app);

app.listen(config.port);