module.exports = {
  redis: {
    url: 'redis://localhost:6379/'
  },
  api: {
    apiURL: '/api',
    baseURL: '/kue',
    updateInterval: 5000
  },
  port: 3000
};
